Shorty Challenge
================

**Challenge accepted! :-)**

### Stack

This project uses [Roda](http://roda.jeremyevans.net/), a simple Ruby Framework built on Rack that performs really well compared to the others. It also uses [Sequel](http://sequel.jeremyevans.net), a simple but powerful database toolkit, and connects to a PostgreSQL db.

As per instructions, Docker and docker-compose were used.

_Note: This was my first time using Roda/Sequel/Docker (it was fun!). I tried to find the best way to implement it, but if anything doesn't look "right" I'd love to learn from you what the correct way is!_

### Running

###### Web server
`docker-compose up`
This command will boot up the web server on `localhost:3000`.

###### Tests
`docker-compose -f docker-compose.yml -f docker-compose.test.yml up`
This command will run the test suite.

### Architecture Summary

The architecture of this project was influenced by Ruby on Rails conventions.

The database is updated through versioned migrations contained in the `/migrate` folder. The models work as an interface to the database and include the most relevant business logic. The `ShortUrl` model stores the mapping between an original url and its shortened code. It includes the appropriate attribute validations and uses `Roda`'s plugins to bring database validations to the model level. Additionally, it has the capabilities of generating a random valid code and uses a before filter to do so when the code is not present.

The `lib` folder contains utility classes with a single responsibility. The `ShortUrlValidationMapper` takes an invalid `ShortUrl` object and maps the correct http code and message for the response.

The `app.rb` leverages Roda's routing tree structure to build the endpoints as described in the API documentation.

Then `bin` folder contains scripts that are used to run the project. The project can be run through two different `docker-compose` configurations. The primary configuration calls `run.sh` to boot up the web server, while the `.test` configuration calls `run.test.sh` to run the test suite. Another script, `wait-for-db.sh` is used to try to open a TCP connection with the database to determine when it's ready to accept connections.

### Considerations

Before shipping this code to production, these are some of the things I'd like to address or discuss with my team:

- If and how we want to validate urls
- Add appropriate logging, monitoring and error reporting
- Determine a styleguide and set up rubocop rules accordingly
- Possibly add API versioning and move the documentation into the code
- How to handle general exceptions in the service

**Thank you for your time!** 


-------------------------------------------------------------------------

## API Documentation

### POST /shorten

```
POST /shorten
Content-Type: "application/json"

{
  "url": "http://example.com",
  "shortcode": "example"
}
```

Attribute | Description
--------- | -----------
**url**   | url to shorten
shortcode | preferential shortcode

##### Returns:

```
201 Created
Content-Type: "application/json"

{
  "shortcode": :shortcode
}
```

A random shortcode is generated if none is requested, the generated short code has exactly 6 alpahnumeric characters and passes the following regexp: ```^[0-9a-zA-Z_]{6}$```.

##### Errors:

Error | Description
----- | ------------
400   | ```url``` is not present
409   | The the desired shortcode is already in use. **Shortcodes are case-sensitive**.
422   | The shortcode fails to meet the following regexp: ```^[0-9a-zA-Z_]{4,}$```.


### GET /:shortcode

```
GET /:shortcode
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**shortcode**  | url encoded shortcode

##### Returns

**302** response with the location header pointing to the shortened URL

```
HTTP/1.1 302 Found
Location: http://www.example.com
```

##### Errors

Error | Description
----- | ------------
404   | The ```shortcode``` cannot be found in the system

### GET /:shortcode/stats

```
GET /:shortcode/stats
Content-Type: "application/json"
```

Attribute      | Description
-------------- | -----------
**shortcode**  | url encoded shortcode

##### Returns

```
200 OK
Content-Type: "application/json"

{
  "startDate": "2012-04-23T18:25:43.511Z",
  "lastSeenDate": "2012-04-23T18:25:43.511Z",
  "redirectCount": 1
}
```

Attribute         | Description
--------------    | -----------
**startDate**     | date when the url was encoded, conformant to [ISO8601](http://en.wikipedia.org/wiki/ISO_8601)
**redirectCount** | number of times the endpoint ```GET /shortcode``` was called
lastSeenDate      | date of the last time the a redirect was issued, not present if ```redirectCount == 0```

##### Errors

Error | Description
----- | ------------
404   | The ```shortcode``` cannot be found in the system


