class App < Roda
  plugin :json
  plugin :halt

  route do |r|
    # POST /shorten
    r.post 'shorten' do
      params = JSON.parse(request.body.read)
      short_url = ShortUrl.new(
        url: params['url'],
        code: params['shortcode'],
        created_at: Time.now
      )

      unless short_url.save(raise_on_failure: false)
        error = ShortUrlValidationMapper.new(short_url)
        r.halt(error.http_code, error: error.message)
      end

      { 'shortcode': short_url.code }
    end

    # GET /:shortcode
    r.get String do |shortcode|
      short_url = ShortUrl.where(code: shortcode).first
      r.halt(404, error: 'Shortcode does not exist') unless short_url

      short_url.update(
        last_seen: Time.now,
        redirect_count: short_url.redirect_count + 1
      )

      r.redirect short_url.url
    end

    # GET /:shortcode/stats
    r.get String, 'stats' do |shortcode|
      short_url = ShortUrl.where(code: shortcode).first
      r.halt(404, error: 'Shortcode does not exist') unless short_url

      short_url.stats
    end
  end
end
