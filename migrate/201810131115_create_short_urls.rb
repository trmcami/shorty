Sequel.migration do
  change do
    create_table(:short_urls) do
      primary_key :id
      String :url, null: false
      String :code, null: false
      Integer :redirect_count, null: false, default: 0
      DateTime :created_at, null: false
      DateTime :last_seen

      index :code, unique: true
    end
  end
end
