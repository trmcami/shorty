FROM ruby:2.5.1

RUN mkdir /app
WORKDIR /app

COPY ./Gemfile      /app
COPY ./Gemfile.lock /app

RUN bundle install

COPY . /app

EXPOSE 3000
