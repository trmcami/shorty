# Map ShortUrl validation errors into the correct http response code and message
class ShortUrlValidationMapper
  # short_url: ShortUrl instance that has been validated
  def initialize(short_url)
    @errors = short_url.errors
  end

  def http_code
    map_validation unless @http_code
    @http_code
  end

  def message
    map_validation unless @message
    @message
  end

  def map_validation
    if @errors.on(:url)&.include? 'is not present'
      set_response(http_code: 400, message: 'Url is not present.')
    elsif @errors.on(:code)&.include? 'is already taken'
      set_response(http_code: 409, message: 'The desired shortcode is already in use.')
    elsif @errors.on(:code)&.include? 'is invalid'
      set_response(http_code: 422, message: 'The shortcode fails to meet regex.')
    else
      set_response(http_code: 500, message: 'Invalid Short Url')
    end
  end

  private

  def set_response(http_code:, message:)
    @http_code = http_code
    @message = message
  end
end
