require 'rack/test'

describe App do
  include Rack::Test::Methods

  describe 'POST /shorten' do
    describe 'with valid params provided' do
      before { post '/shorten', { url: 'http://shorty.com', shortcode: 'ABC123' }.to_json }

      it 'is successful' do
        expect(last_response.status).to eq(200)
      end

      it 'returns the shortcode' do
        expect(json_response).to eq('shortcode' => 'ABC123')
      end

      it 'has the correct header' do
        expect(last_response.headers['Content-Type']).to eq('application/json')
      end
    end

    describe 'without code provided' do
      before { post '/shorten', { url: 'http://shorty.com' }.to_json }

      it 'is successful' do
        expect(last_response.status).to eq(200)
      end

      it 'returns generated shortcode' do
        expect(json_response['shortcode']).not_to be_empty
      end
    end

    describe 'with error' do
      before { post '/shorten', { url: 'http://shorty.com', shortcode: 'ABC' }.to_json }

      it 'returns correct http code' do
        expect(last_response.status).to eq(422)
      end

      it 'returns error' do
        expect(json_response['error']).not_to be_empty
      end
    end
  end

  describe 'GET /:shortcode' do
    let(:url) { 'http://shorty.com' }
    let(:code) { 'ABC123' }

    describe 'short url exists' do
      let!(:short_code) { ShortUrl.new(url: url, code: code, redirect_count: 1).save }

      before do
        get "/#{code}"
      end

      it 'has redirect http code' do
        expect(last_response.status).to eq(302)
      end

      it 'redirects to url' do
        expect(last_response.headers['Location']).to eq(url)
      end

      it 'increments redirect counter' do
        expect(short_code.reload.redirect_count).to eq(2)
      end
    end

    describe 'short url does not exist' do
      before do
        get "/#{code}"
      end

      it 'has correct http code' do
        expect(last_response.status).to eq(404)
      end

      it 'returns error' do
        expect(json_response['error']).not_to be_empty
      end
    end
  end

  describe 'GET /:shortcode/stats' do
    let(:url) { 'http://shorty.com' }
    let(:code) { 'ABC123' }
    let(:created_at) { Time.parse('2017-10-10') }
    let(:last_seen) { Time.parse('2017-10-14') }
    let(:redirect_count) { 5 }

    describe 'short url exists' do
      let!(:short_code) { ShortUrl.new(url: url, code: code, redirect_count: redirect_count, created_at: created_at, last_seen: last_seen).save }

      before do
        get "/#{code}/stats"
      end

      it 'has correct http code' do
        expect(last_response.status).to eq(200)
      end

      it 'has start date' do
        expect(json_response['startDate']).to eq(created_at.iso8601)
      end

      it 'has last seen date' do
        expect(json_response['lastSeenDate']).to eq(last_seen.iso8601)
      end

      it 'has redirect count' do
        expect(json_response['redirectCount']).to eq(redirect_count)
      end

      it 'has the correct header' do
        expect(last_response.headers['Content-Type']).to eq('application/json')
      end
    end

    describe 'short url does not exist' do
      before do
        get "/#{code}/stats"
      end

      it 'has correct http code' do
        expect(last_response.status).to eq(404)
      end

      it 'returns error' do
        expect(json_response['error']).not_to be_empty
      end
    end

    describe 'short url was never visited' do
      let(:last_seen) { nil }
      let(:redirect_count) { 0 }

      let!(:short_code) { ShortUrl.new(url: url, code: code, redirect_count: redirect_count, created_at: created_at, last_seen: last_seen).save }

      before do
        get "/#{code}/stats"
      end

      it 'does not have last seen date' do
        expect(json_response.has_key?('lastSeenDate')).to be false
      end
    end
  end

  def app
    App
  end
end
