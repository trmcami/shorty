describe 'ShortUrl' do
  describe 'validations' do
    describe 'invalid code format' do
      let(:short_url) { ShortUrl.new(url: 'http://shorty.com', code: 'ABC') }

      it 'is invalid' do
        expect(short_url.valid?).to be false
      end
    end

    describe 'code is case sensitive' do
      let(:code) { 'ABC123' }
      let(:short_url) { ShortUrl.new(url: 'http://shorty.com', code: 'ABC123') }

      before do
        ShortUrl.new(url: 'http://shorty.com', code: 'abc123').save
      end

      it 'is valid' do
        expect(short_url.valid?).to be true
      end
    end
  end

  describe 'before_create' do
    let(:url) { 'http://www.shorty.com' }
    let(:short_url) { ShortUrl.new(url: url) }

    it 'generates unique valid token' do
      expect(short_url.valid?).to be true
      expect(short_url.code).not_to be_nil
    end

    it 'keeps correct url' do
      expect(short_url.valid?).to be true
      expect(short_url.url).to eq(url)
    end

    describe 'code is already in use' do
      let(:code_in_use) { 'ABC123' }
      let(:new_code) { 'DEF456' }

      before do
        ShortUrl.new(url: 'http://shorty.com', code: code_in_use).save
        allow(ShortUrl).to receive(:generate_code).and_return(code_in_use, new_code)
      end

      it 'generates new unique token' do
        expect(short_url.valid?).to be true
        expect(short_url.code).not_to be_nil
        expect(short_url.code).to eq(new_code)
      end
    end

    describe 'url missing http' do
      let(:url) { 'shorty-missing.com' }

      it 'adds http://' do
        expect(short_url.valid?).to be true
        expect(short_url.url).to eq('http://shorty-missing.com')
      end
    end
  end
end
