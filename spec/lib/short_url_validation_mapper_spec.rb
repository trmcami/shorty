describe 'ShortUrlValidationMapper' do
  describe 'map_validations' do
    before { short_url.valid? }
    let(:validation_mapper) { ShortUrlValidationMapper.new(short_url) }

    describe 'missing url' do
      let(:short_url) { ShortUrl.new(url: nil) }

      before do
        short_url.valid?
      end

      it 'returns correct http code' do
        expect(validation_mapper.http_code).to eq(400)
      end
    end

    describe 'code is already in use' do
      let(:code_in_use) { 'ABC123' }
      let(:short_url) { ShortUrl.new(url: 'http://newshorty.com', code: code_in_use) }

      before do
        ShortUrl.new(url: 'http://shorty.com', code: code_in_use).save
        short_url.valid?
      end

      it 'returns correct http code' do
        expect(validation_mapper.http_code).to eq(409)
      end
    end

    describe 'invalid code' do
      let(:short_url) { ShortUrl.new(url: 'http://shorty.com', code: 'ABC') }

      before do
        short_url.valid?
      end

      it 'returns correct http code' do
        expect(validation_mapper.http_code).to eq(422)
      end
    end
  end
end
