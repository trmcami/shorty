#!/bin/sh
# Script to wait until database is ready to accept connections.
# Based on Docker Docs (https://docs.docker.com/compose/startup-order/) but using ruby sockets

hostport=(${1//:/ })
host=${hostport[0]}
port=${hostport[1]}
shift
cmd="$@"

until ruby -rsocket -e 's=TCPSocket.new($*[0],$*[1])' -- $host $port;
do
  >&2 echo "DB not accepting connections - sleeping"
  sleep 2;
done

>&2 echo "DB is up - starting"
exec $cmd
