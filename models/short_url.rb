class ShortUrl < Sequel::Model
  plugin :auto_validations
  plugin :validation_class_methods

  CODE_CHARS = (Array('a'..'z') + Array('A'..'Z') + Array('0'..'9') + ['_']).freeze
  VALID_CODE_REGEX = /^[0-9a-zA-Z_]{4,}$/

  validates_format_of :code, with: VALID_CODE_REGEX

  def self.generate_code
    CODE_CHARS.sample(6).join
  end

  def stats
    {
      startDate: created_at.iso8601,
      redirectCount: redirect_count,
      lastSeenDate: (last_seen.iso8601 if redirect_count > 0)
    }.compact
  end

  private

  def before_validation
    set_unique_code
    format_url
    self.created_at ||= Time.now
    super
  end

  def set_unique_code
    return if code
    self.code = loop do
      random_code = ShortUrl.generate_code
      break random_code unless ShortUrl.where(code: random_code).first
    end
  end

  def format_url
    return unless url
    self.url = "http://#{url}" unless url.downcase.start_with?("http")
  end
end
