require 'roda'
require 'sequel'
require_relative 'app'
require 'byebug'

DB = Sequel.connect(ENV['DATABASE_URL'])

Dir['./models/*.rb'].each { |f| require f }
Dir['./lib/*.rb'].each { |f| require f }
